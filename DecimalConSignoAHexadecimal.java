/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesm.hexadecimal;

/**
 *
 * @author Luis
 */
public class DecimalConSignoAHexadecimal{
    private String numDecimal;
    
    public String conversionConSignoYSinSigno(String numDecimal){
        String resultado = "";
                int numDeci = Integer.parseInt(numDecimal);
                int valorAbsoluto = Math.abs(numDeci);
                if(getNumDecimal().substring(0,1).equals("-")){
                    resultado = convercionDecimalAHexadecimal(valorAbsoluto+"");
                    resultado = complementoADosDeHexadecial(resultado);
                }else{                    
                    resultado = convercionDecimalAHexadecimal(valorAbsoluto+"");
                }
                
        return resultado;
    }
    
    
    public String convercionDecimalAHexadecimal(String numDecimal){
            String resultado = "";
            int decimal = Integer.parseInt(numDecimal);
            String[] numHexa = new String[numDecimal.length()+1];
            int residuo;
            int x = 0;
            do{
                residuo = decimal % 16;
                          decimal = decimal /16;                
                if(residuo > 9 && residuo < 16){
                    numHexa[x] = conversion(residuo+"");
                    x++;
                }else{
                    numHexa[x] = residuo+"";
                    x++;
                }                        
              }while(residuo != 0 && residuo != 1);               
             for(int y = numHexa.length-1; y >= 0; y--){            
                if(!(numHexa[y] == null)){
                  resultado += numHexa[y];
                }
        }
        return resultado;
    }
    
     public String conversion(String numHexadecimal){  
        int num = Integer.parseInt(numHexadecimal);     
        String letra ="";
        switch(num){
            case 10: letra = "A";break;
            case 11: letra = "B";break;
            case 12: letra = "C";break;
            case 13: letra = "D";break;
            case 14: letra = "E";break;    
            case 15: letra = "F";   
        }      
        return letra;
    }

    public String getNumDecimal() {
        return numDecimal;
    }
    public void setNumDecimal(String numDecimal) {
        this.numDecimal = numDecimal;
    }
    
     public String complementoADosDeHexadecial(String numHexa){
        String [] res = new String[numHexa.length()];
        String numeroHexa = "";
        String compleADos = "";
        int pos = 0;
            for(int x = numHexa.length(); x > 0; x--){
                res[pos] = numHexa.substring(x-1, x);                              
                res[pos]=  restar(res[pos]);                
                pos++;
            }
            for(int y = res.length; y > 0; y--){
                numeroHexa += res[y-1];
            }
         
           compleADos = sumar(numeroHexa,"000001");              
         
        return  compleADos;
    }
    
    public String restar(String numHexa){
        char[] letra = numHexa.toCharArray();        
        switch(letra[0]){
            case '0': return "F";
            case '1': return "E";
            case '2': return "D";
            case '3': return "C";
            case '4': return "B";
            case '5': return "A";
            case '6': return "9";
            case '7': return "8";
            case '8': return "7";
            case '9': return "6";
            case 'A': return "5";
            case 'B': return "4";
            case 'C': return "3";
            case 'D': return "2";
            case 'E': return "1";
            case 'F': return "0";
        }
        return "";
    }
    public String sumar(String numHexaUno, String numHexaDos){
            String resSuma ="";
            String [] numUno = new String[numHexaUno.length()];
            String [] numDos = new String[numHexaDos.length()];
            int tamanio = (numHexaUno.length() >= numHexaDos.length()) ? numHexaUno.length()+1 : numHexaDos.length()+1;
            String[] suma = new String[tamanio];
            int posUno = 0;
            int posDos = 0;
       
            for(int x = tamanio-1; x > 0; x--){                
                try{                          
                    if(numHexaUno.substring(x-1, x)!= null){numUno[posUno] = numHexaUno.substring(x-1, x);                       
                       numUno[posUno] = convertirHexaDeci(numUno[posUno]);
                       posUno++;
                    }else{numUno[posUno] = "0";posUno++;}
                    if(numHexaDos.substring(x-1, x)!= null){numDos[posDos] = numHexaDos.substring(x-1, x);                       
                       numDos[posDos] = convertirHexaDeci(numDos[posDos]);
                       posDos++;
                    }else{numDos[posDos] = "0";posDos++;}     
                }catch(Exception e){}
            }
            for(int y = 0; y < tamanio-1; y++ ){
                int valorUno =0;
                int valorDos =0;
                try{                    
                    if(numUno[y] != null){valorUno = Integer.parseInt(numUno[y]);}
                    if(numDos[y] != null){valorDos = Integer.parseInt(numDos[y]);}
                    suma[y] = valorUno+valorDos+"";
                }catch(Exception ee){}             
            }
            for(int q = 0; q < tamanio-1; q++){
                if(suma[q] != null){
                  int nume = Integer.parseInt(suma[q]);  
                    int valor;
                    if( nume > 15){                      
                        valor = (Integer.parseInt(suma[q]) - 16);               
                        suma[q] = valor+"";            
                        int nuevoValor = (Integer.parseInt(suma[q+1])+1);
                        suma[q+1] = nuevoValor+"";                      
                    }
                }                 
    
             }  
            
            suma[0] = (Integer.parseInt(suma[0])+1)+"";
             for(int v = 0; v < suma.length; v++){
               if(suma[v] != null){
                   int num = Integer.parseInt(suma[v]);
                   suma[v] = convertirDeciHexa(num)+"";                  
               }
             }
              for(int z = suma.length-1; z >= 0; z--){
               if(suma[z] != null){
                    resSuma += suma[z]+"";
                   
               }
               
           }
          
        return resSuma;
    }
    
     public String convertirHexaDeci(String numHexa){
        char[] letra = numHexa.toCharArray();        
        switch(letra[0]){
            case '0': return "0";
            case '1': return "1";
            case '2': return "2";
            case '3': return "3";
            case '4': return "4";
            case '5': return "5";
            case '6': return "6";
            case '7': return "7";
            case '8': return "8";
            case '9': return "9";
            case 'A': return "10";
            case 'B': return "11";
            case 'C': return "12";
            case 'D': return "13";
            case 'E': return "14";
            case 'F': return "15";
        }
        return "";
    }
     public String convertirDeciHexa(int num){
         switch(num){
            case 0: return "0";
            case 1: return "1";
            case 2: return "2";
            case 3: return "3";
            case 4: return "4";
            case 5: return "5";
            case 6: return "6";
            case 7: return "7";
            case 8: return "8";
            case 9: return "9";
            case 10: return "A";
            case 11: return "B";
            case 12: return "C";
            case 13: return "D";
            case 14: return "E";
            case 15: return "F";
        }
        return "";
    }
     
     public static void main(String[] args){
         DecimalConSignoAHexadecimal conversion = new DecimalConSignoAHexadecimal();
         conversion.setNumDecimal("250");
         System.out.println("Ejemplo con signo positivo");
         System.out.println("Convertir de Decimal con signo a Hexadecimal");
         System.out.println("Número decimal           = "+conversion.getNumDecimal()
                           +"\nConvertido a Hexadecimal = "+conversion.conversionConSignoYSinSigno(conversion.getNumDecimal()));
         
         System.out.println("Ejemplo con signo negativo");
         conversion.setNumDecimal("-250");
         System.out.println("Número decimal           = "+conversion.getNumDecimal()
                           +"\nConvertido a Hexadecimal = "+conversion.conversionConSignoYSinSigno(conversion.getNumDecimal()));
         
         
     }
    
}
